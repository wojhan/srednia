﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Srednia
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private int analiza = 6, algebra = 6, bhp = 1, logika = 3, pp = 8, wdi = 4, linux = 2, fizyka = 2, angielski = 2, dyskretna = 4, statystyka = 4, npto = 3, peie = 5, po = 6, tc = 4;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double analizaT = ((Convert.ToDouble(analizaW.Text) + Convert.ToDouble(analizaC.Text)) / 2) * analiza;
            double algebraT = ((Convert.ToDouble(algebraW.Text) + Convert.ToDouble(algebraC.Text)) / 2) * algebra;
            double bhpT = (Convert.ToDouble(bhpW.Text)) * bhp;
            double logikaT = ((Convert.ToDouble(logikaW.Text) + Convert.ToDouble(logikaC.Text)) / 2) * logika;
            double ppT = ((Convert.ToDouble(ppW.Text) + Convert.ToDouble(ppC.Text) + Convert.ToDouble(ppP.Text)) / 3) * pp;
            double wdiT = ((Convert.ToDouble(wdiW.Text) + Convert.ToDouble(wdiC.Text)) / 2) * wdi;
            double linuxT = ((Convert.ToDouble(linuxW.Text) + Convert.ToDouble(linuxP.Text)) / 2) * linux;
            if (pierwszysemestr.IsChecked.Value)
            {
                srednia.Content = ((analizaT + algebraT + bhpT + logikaT + ppT + wdiT + linuxT) / 30).ToString();
            }
        }
    }
}
